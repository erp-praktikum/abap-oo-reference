# README #

### What is this repository for? ###

This repository contains reference code, solutions, for the abap oo part of "Praktikum zu ERP-Systemen" for university Passau.

### How do I get set up? ###

- Create parent package in ABAP system (Z_ABAPOO_REFERENCE)
- use abapgit to pull

### Who do I talk to? ###

Responsible: thomas.kruegl@msg.group