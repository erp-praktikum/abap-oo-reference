CLASS z_abapoo_singleton DEFINITION
  PUBLIC
  FINAL
  CREATE PRIVATE .

  PUBLIC SECTION.
    DATA shared_text  TYPE string VALUE 'initial value'.
    CLASS-METHODS class_constructor.

    CLASS-METHODS get_instance
      RETURNING VALUE(re_instance) TYPE REF TO z_abapoo_singleton.

  PRIVATE SECTION.
    CLASS-DATA instance TYPE REF TO z_abapoo_singleton.

ENDCLASS.



CLASS Z_ABAPOO_SINGLETON IMPLEMENTATION.


  METHOD class_constructor.
    z_abapoo_singleton=>instance = NEW #(  ).
  ENDMETHOD.


  METHOD get_instance.
    re_instance = z_abapoo_singleton=>instance.
  ENDMETHOD.
ENDCLASS.
