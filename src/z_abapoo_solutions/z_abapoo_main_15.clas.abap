CLASS z_abapoo_main_15 DEFINITION
  PUBLIC
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.

    INTERFACES if_oo_adt_classrun .
  PROTECTED SECTION.
  PRIVATE SECTION.
ENDCLASS.



CLASS z_abapoo_main_15 IMPLEMENTATION.


  METHOD if_oo_adt_classrun~main.
    DATA: l_private_singleton1 TYPE REF TO z_abapoo_private_singleton,
          l_private_singleton2 TYPE REF TO z_abapoo_private_singleton,
          l_friend             TYPE REF TO z_abapoo_friend.

    l_private_singleton1 = z_abapoo_private_singleton=>get_instance( ).
    l_private_singleton2 = z_abapoo_private_singleton=>get_instance( ).

    out->write( l_private_singleton1->get_shared_text( ) ).
    out->write( l_private_singleton2->get_shared_text( ) ).

    l_friend = NEW #(  ).
    l_friend->set_shared_text( 'new text' ).

    out->write( l_private_singleton1->get_shared_text( ) ).
    out->write( l_private_singleton2->get_shared_text( ) ).

  ENDMETHOD.
ENDCLASS.
