CLASS z_abapoo_06_data_gen DEFINITION
  PUBLIC
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.

    INTERFACES if_oo_adt_classrun .
  PROTECTED SECTION.
  PRIVATE SECTION.
ENDCLASS.



CLASS Z_ABAPOO_06_DATA_GEN IMPLEMENTATION.


  METHOD if_oo_adt_classrun~main.
    TYPES ltt_airplanes TYPE STANDARD TABLE OF zoo_airplane WITH DEFAULT KEY.

    DATA(l_airplanes) = VALUE ltt_airplanes(
        ( type_id = '767-200' weight = 99999 )
        ( type_id = 'A320-200' weight = 70000 )
        ( type_id = '747-400' weight = 250000 )
     ).

    " clear table first to avoid duplicates on multiple runs
    DELETE FROM zoo_airplane.

    INSERT zoo_airplane FROM TABLE @l_airplanes.

  ENDMETHOD.
ENDCLASS.
