CLASS z_abapoo_private_singleton DEFINITION
  PUBLIC
  FINAL
  CREATE PRIVATE
  GLOBAL FRIENDS z_abapoo_friend.

  PUBLIC SECTION.

    METHODS get_shared_text RETURNING VALUE(re_shared_text) TYPE string.

    CLASS-METHODS class_constructor.

    CLASS-METHODS get_instance
      RETURNING VALUE(re_instance) TYPE REF TO z_abapoo_private_singleton.

  PRIVATE SECTION.
    DATA shared_text  TYPE string VALUE 'initial value'.
    CLASS-DATA instance TYPE REF TO z_abapoo_private_singleton.

ENDCLASS.



CLASS z_abapoo_private_singleton IMPLEMENTATION.


  METHOD class_constructor.
    z_abapoo_private_singleton=>instance = NEW #(  ).
  ENDMETHOD.


  METHOD get_instance.
    re_instance = z_abapoo_private_singleton=>instance.
  ENDMETHOD.

  METHOD get_shared_text.
    re_shared_text = me->shared_text.
  ENDMETHOD.

ENDCLASS.
