CLASS z_abapoo_main_14 DEFINITION
  PUBLIC
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.

    INTERFACES if_oo_adt_classrun .
  PROTECTED SECTION.
  PRIVATE SECTION.
ENDCLASS.



CLASS Z_ABAPOO_MAIN_14 IMPLEMENTATION.


  METHOD if_oo_adt_classrun~main.
    DATA: l_singleton1 TYPE REF TO z_abapoo_singleton,
          l_singleton2 TYPE REF TO z_abapoo_singleton.

    l_singleton1 = z_abapoo_singleton=>get_instance( ).

    l_singleton2 = z_abapoo_singleton=>get_instance( ).

    out->write( l_singleton1->shared_text ).
    out->write( l_singleton2->shared_text ).

    l_singleton1->shared_text = 'new text'.

    out->write( l_singleton1->shared_text ).
    out->write( l_singleton2->shared_text ).


  ENDMETHOD.
ENDCLASS.
