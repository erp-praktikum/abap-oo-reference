CLASS zcx_oo_unknown_planetype_2 DEFINITION
  PUBLIC
  INHERITING FROM cx_static_check
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.

    INTERFACES if_t100_dyn_msg .
    INTERFACES if_t100_message .

    CONSTANTS:
      BEGIN OF zcx_oo_unknown_planetype_2,
        msgid TYPE symsgid VALUE 'Z_ABAPOO_TEXTS',
        msgno TYPE symsgno VALUE '001',
        attr1 TYPE scx_attrname VALUE '',
        attr2 TYPE scx_attrname VALUE '',
        attr3 TYPE scx_attrname VALUE '',
        attr4 TYPE scx_attrname VALUE '',
      END OF zcx_oo_unknown_planetype_2.

    DATA planetype TYPE /dmo/plane_type_id.

    METHODS constructor
      IMPORTING
        !textid      LIKE if_t100_message=>t100key OPTIONAL
        !previous    LIKE previous OPTIONAL
        im_planetype TYPE /dmo/plane_type_id OPTIONAL.
  PROTECTED SECTION.
  PRIVATE SECTION.
ENDCLASS.



CLASS zcx_oo_unknown_planetype_2 IMPLEMENTATION.


  METHOD constructor ##ADT_SUPPRESS_GENERATION.
    CALL METHOD super->constructor
      EXPORTING
        previous = previous.
    CLEAR me->textid.
    IF textid IS INITIAL.
      if_t100_message~t100key = me->zcx_oo_unknown_planetype_2.
      if_t100_message~t100key-attr1 = im_planetype.
    ELSE.
      if_t100_message~t100key = textid.
    ENDIF.
    me->planetype = im_planetype.
  ENDMETHOD.
ENDCLASS.
