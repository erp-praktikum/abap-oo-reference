CLASS z_abapoo_friend DEFINITION
  PUBLIC
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.
    METHODS set_shared_text IMPORTING im_new_shared_text TYPE string.
  PROTECTED SECTION.
  PRIVATE SECTION.
ENDCLASS.



CLASS z_abapoo_friend IMPLEMENTATION.
  METHOD set_shared_text.
    DATA(l_singleton) = z_abapoo_private_singleton=>get_instance(  ).
    l_singleton->shared_text = im_new_shared_text.
  ENDMETHOD.

ENDCLASS.
